const express = require('express')
const router = express.Router()
const {getAll, getById, save} = require('../controllers/eventosController')


module.exports = (repository) => {

    router.get('/', getAll(repository) )

    router.get('/:id', getById(repository) )

    router.post('/', save(repository) )

    return router;
}


