const axios = require('axios').default;

// creación de una función que devuelve una promesa
function obtenerSeguidores(nombre_usuario) {
  return new Promise((resolve, reject) => {
    axios.get("https://api.github.com/users/" + nombre_usuario)  // usamos fetch en el browser como ejemplo
      .then((githubUser) => {
        axios.get(githubUser.data.followers_url)
          .then((followers) => {
            resolve(followers.data);
          })
      })
      .catch((error) => {
        console.log(error)
        reject("se produjo un error");
      })
  })
}


// llamado a una función que devuelve una promesa
obtenerSeguidores("smartNetAr")
  .then((seguidores) => {
    for (let i = 0; i < 3; i++) {
      console.log(seguidores[i]?.login)
    }
  })
  .catch((error) => console.log(error))