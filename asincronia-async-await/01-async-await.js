const axios = require('axios')

async function obtenerFollowers( nombre_usuario ){
    try {
      const respuestaUsuario = await axios.get("https://api.github.com/users/"+nombre_usuario)
      if (!respuestaUsuario) {
          throw Error('No se encuentra el usuario')
      }
      const datos = respuestaUsuario.data
      const respuestaFollowers = await axios.get(datos.followers_url)
      const followers = respuestaFollowers.data
      return followers
    }
    catch (error) {
      throw Error(error)
    }
 
 }
 
 obtenerFollowers("smartnetar111111111")
   .then( misFollowers => { console.log("Followers:", misFollowers)})
   .catch( error => console.log("ERROR!!!", error))
