
const fakeEvents = [
    {
        id: 1,
        nombre: 'Evento 1',
        fecha: '2020-01-01',
        hora: '10:00',
    },
    {
        id: 2,
        nombre: 'Evento 2',
        fecha: '2020-01-01',
        hora: '10:00',
    },
]

exports.getAll = async () => {
    return fakeEvents;
}

exports.getById = async (id) => {
    return fakeEvents.find(evento => evento.id == id);
}

exports.save = async (event) => {
    id = fakeEvents[fakeEvents.length - 1].id + 1;
    return fakeEvents.push({id, ...event});
}

exports.getByName = async (nombre) => {
    return fakeEvents.find(evento => evento.nombre == nombre);
}

