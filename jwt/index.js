const express = require('express')
var jwt = require('jsonwebtoken');
const app = express()
const port = 3000

app.use(express.json())

app.get('/', (req, res) => {
  res.send('Hello World!')
})

const authMiddleware = (req, res, next) => {
    const jwtToken = req.headers.authorization;
    const token = jwtToken.split(" ")[1];

    try {
        const decoded = jwt.verify(token, 'otra_cosa');
        req.userEmail = decoded.email;
        next()
    } catch (error) {
        res.status(403).json({data: "token inválido"});
    }
}

app.post('/login', (req, res) => {
    const validEmail = "leo@email.com";
    const validPassword = "1234"

    const {email, password} = req.body;

    if (validEmail === email && validPassword === password)
    {

        const token = jwt.sign({ id: 123, name: "juan martinez", email }, 'otra_cosa');
        res.json({data: {
            token
        }})
    }
    else
    {
        res.status(401).json({data: "credenciales inválidas"});
    }
})


app.get("/ruta-segura", authMiddleware, (req, res) => 
{
   res.json({data: `info de ruta segura para el email válido ${req.userEmail}`})
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})
