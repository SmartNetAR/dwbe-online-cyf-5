const redis = require('redis');

const client = redis.createClient({
    host: 'localhost',
    port: 6379,
    password: 'secret'
});

client.on('connect', () => {
    console.log('Redis client connected');
});

client.on('error', (err) => {
    console.log('Something went wrong ' + err);
});

// client.set('nombre', 'Agustin');
const user = {
    name: 'Agustin',
    lastName: 'Gonzalez'
};

client.set('user', JSON.stringify(user), 'EX', 15);

client.get('user', (err, reply) => {
    if (err) {
        console.log(err);
    } else {
        if (reply == null) {
            console.log('No existe el valor');
        } else {
            const userCache = JSON.parse(reply);
            console.log(userCache.lastName);
            console.log('el valor es: ', reply);
        }
    }
});