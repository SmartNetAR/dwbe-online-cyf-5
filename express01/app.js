const config = require('./config');
const express = require('express');
const marcasAutos = require('./data');
const cors = require('cors');

const app = express()

app.use(cors())

app.use(express.json())


function validarIdAuto(req, res, next) {
    const idAuto = parseInt(req.params.id);

    if ( !Number.isInteger(idAuto))
    {
        res.status(422).json({msg: "El id debe ser un número entero"})
        return
    }
    else {
        next()
    }
}



app.get('/', (req, res) => {
    console.log("req.path")
    console.log(req.method)
    console.log(req.query)
    res.status(200).send(marcasAutos)
})

app.get('/autos', (req, res) => {
    console.log(req.header('user-id'))
    const auto = marcasAutos;
    
    res.status(200).json(auto);
})


app.get('/autos/:id', validarIdAuto, (req, res) => {

    const idAuto = parseInt(req.params.id);

    const auto = marcasAutos.find(auto => auto.id === idAuto)

    if (auto) {
        res.status(200).json(auto);
    } else {
        res.status(400).json(`No se encuentra el auto con id ${idAuto}`)
    }
})

app.delete('/autos/:id', validarIdAuto, (req, res) => {

    // obtenermos el id de la ruta
    const idAuto = parseInt(req.params.id);
    
    // buscamos si el auto existe
    const auto = marcasAutos.find(auto => auto.id === idAuto);
    // si no existe damos un error
    if (!auto) {
        res.status(400).json(`No se encuentra el auto con id ${idAuto}`)
        return
    }

    // elimiar el auto
    const posAutoId = marcasAutos.indexOf(auto)

    marcasAutos.splice(posAutoId, 1)
    
    res.json(marcasAutos)
})


app.post('/autos', (req, res) => {
    const auto = {};
    auto.id = marcasAutos[marcasAutos.length -1].id + 1;
    auto.nombre = req.body.nombre;
    auto.modelo = req.body.modelo;
    marcasAutos.push(auto);
    res.status(201).json({auto})
})

app.listen(config.port, () => {
  console.log(`Example app listening at http://localhost:${config.port}`)
})