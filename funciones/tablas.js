
const imprimirResultadoMultiplicado = ( tabla, multiplicador ) =>
{
  console.log(tabla, " x ", multiplicador, " = ", tabla * multiplicador)
}

function imprimirTabla( tabla ) {
  for ( let multiplica = 1; multiplica <= 10; multiplica++ ) {
    imprimirResultadoMultiplicado( tabla, multiplica )
  }
}

imprimirTabla(3)
imprimirResultadoMultiplicado( 11, 55 )
imprimirTabla(15)

