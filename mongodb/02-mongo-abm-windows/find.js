const mongoose = require('./connection');

const Pelicula = mongoose.model('peliculas', {
    titulo: String,
    director: String,
    genero: String,
    lanzamiento: Date
})

Pelicula.findOne({titulo: 'Hombre Araña'})
    .then((res) => {
        console.log('La película encontrada es:');
        console.log(res);
    })