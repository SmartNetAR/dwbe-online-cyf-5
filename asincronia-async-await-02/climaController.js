const { default: axios } = require("axios");
const config = require("./config");
const citys = require("./data");
const getRandomArrayItem = require("./helpers/getRandomArrayItem");

const getCityTemp = async (cityName) => {
    const baseApi = 'http://api.openweathermap.org'
    const apiKey = config.apiKey;

    const respuesta = await axios.get(`${baseApi}/data/2.5/weather?q=${cityName}&appid=${apiKey}&units=metric`)
    return respuesta.data.main.temp;
}


async function climaController(req, res) {
    let arrayCitys = [];
    let eliminarRepetidos = arr => [...new Set(arr)];

    let newArrayCitys = eliminarRepetidos(arrayCitys);

    while (newArrayCitys.length < 3) {
        const randomCity = getRandomArrayItem(citys);
        newArrayCitys.push(randomCity);
        newArrayCitys = eliminarRepetidos(newArrayCitys);
    };

    try {
        for await (let city of newArrayCitys) {
            city.temperatura = await getCityTemp(city.name);
        }
        res.status(200).json({ "response": newArrayCitys });
    } catch (error) {
        res.status(500).json({ "response": error.message });
    }
}

module.exports = climaController;