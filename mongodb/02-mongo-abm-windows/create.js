const mongoose = require('./connection');

const Pelicula = mongoose.model('peliculas', {
    titulo: String,
    director: String,
    otroCampo: String,
    lanzamiento: Date
})

const rsPelicula = new Pelicula({
    titulo: 'Segunda Película',
    director: 'Steven Spielberg',
    otroCampo: 'texto de prueba',
    lanzamiento: new Date()
})

rsPelicula.save()