
const miAuto = {
    marca: "Peugeot",
    modelo: "Partner",
    anyo: 2020,
    dominio: "AB123CD",
    _alquilado: false,
    alquilar: function() {
        if (this._alquilado) {
            throw new Error("No se puede alquilar")
        }
        this._alquilado = true
        return this._alquilado
    },
    devolver: function() {
        this._alquilado = false
        return this._alquilado
    }
}

const autoDeDolores = {
    marca: "Ford",
    modelo: "Fiesta",
    anyo: 2020,
    dominio: "AB124CD",
    alquilado: false,
    alquilar: function() {
        if (this._alquilado) {
            throw new Error("No se puede alquilar")
        }
        this._alquilado = true
        return this._alquilado
    },
    devolver: function() {
        this._alquilado = false
        return this._alquilado
    }
}


// console.log(miAuto)
// console.log(autoDeDolores)

const garage = [miAuto, autoDeDolores]

function publicarAuto(auto) {
    console.log(`Se vende el auto marca: ${auto.marca}, año de fabricación: ${auto.anyo}`)
}


do {
    let nuevoAuto = {}
    nuevoAuto.marca = prompt("Ingrese la marca de su auto");
    nuevoAuto.modelo = prompt("Ingrese el modelo de su auto");
    nuevoAuto.anyo = prompt("Ingrese el año de su auto");
    nuevoAuto.dominio =  prompt("Ingrese el dominio de su auto");
    nuevoAuto._alquilado = false
    garage.push(nuevoAuto)
} while (window.confirm("¿Desea seguir agregando autos al garage?"));



for (let index = 0; index < garage.length; index++) {
    const autoDelGarage = garage[index];

    publicarAuto(autoDelGarage)
    
}

