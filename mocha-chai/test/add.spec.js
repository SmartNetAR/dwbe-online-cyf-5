
const add = require('../src/add');
const assert = require('assert');
const expect = require('chai').expect;


describe('test de sumas', function() {

    describe('#add.js', function() {
        it('Debería devolver cero cuando los dos números sean 0', function() {
            assert.equal(add(0, 0), 0);
         })

        it('Debería devolver el valor 10 cuando se le pase 4 y 6', function() {
            assert.equal(add(4, 6), 10);
        })

        it('Debería devolver el valor -2 cuando se le pase -1 y -1', function() {
            // arrange
            const a = -1;
            const b = -1;

            // act
            const result = add(a, b);

            // assert
            expect(result).to.equal(-2);
        })
    })

})