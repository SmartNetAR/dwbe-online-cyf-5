const express = require('express')
const { config } = require('./config')
const app = express()
app.use(express.json())

const errorHandler = require('./errorHandler')

app.use(errorHandler);

const eventosRepository = require('./eventos/repositories/eventosSqlRepository')
const eventosRoutes  = require('./eventos/routes/eventosRoutes')

app.use('/eventos', eventosRoutes(eventosRepository))

app.listen(config.server.port, () => {
  console.log(`Example app listening at http://localhost:${config.server.port}`)
})
