CREATE DATABASE acamica;

USE acamica;

CREATE TABLE usuarios ( nombre varchar(100), apellido varchar(100), edad int(5) );

DROP TABLE usuarios;

SHOW TABLES;

CREATE TABLE usuarios ( id INT AUTO_INCREMENT, nombre varchar(100), apellido varchar(100), edad int(5), PRIMARY KEY (id) ); 

CREATE TABLE acamica.cursos (
	id INT auto_increment NULL,
	nombre varchar(100) NULL,
	CONSTRAINT cursos_PK PRIMARY KEY (id)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8mb4
COLLATE=utf8mb4_0900_ai_ci;

DESCRIBE cursos;

SELECT * FROM cursos;

SELECT * FROM usuarios;

INSERT INTO cursos (nombre) VALUE ( 'dwbe' );
INSERT INTO cursos (nombre) VALUE ( 'dwfs' );

SELECT nombre FROM cursos WHERE id = 1;

SELECT nombre FROM cursos WHERE id = 1 OR nombre = "dwfs";