## NETWORK
```bash
docker network create app-tier --driver bridge
```

## SERVER
```bash
docker run --rm --name redis-server \
-e REDIS_PASSWORD=secret -e REDIS_PORT_NUMBER=6379 -p 6379:6379 \
--network app-tier \
bitnami/redis:latest
```



## CLI
```bash
docker run -it --rm \
    --network app-tier \
    bitnami/redis:latest redis-cli -h redis-server
```
```bash
AUTH secret
```
