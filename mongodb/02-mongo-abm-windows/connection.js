const mongoose = require('mongoose');

const options = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    // useFindAndModify: false,
    // useCreateIndex: true
}

mongoose.connect('mongodb://localhost:27017/mydb', options);

module.exports = mongoose;