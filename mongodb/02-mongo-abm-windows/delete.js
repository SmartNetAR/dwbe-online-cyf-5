const mongoose = require('./connection')

const Pelicula = mongoose.model('peliculas', {
    titulo: String,
    director: String,
    genero: String,
    lanzamiento: Date
})

Pelicula.deleteOne({_id: '613812df2fb16cbc8176d4e5'})
    .then( (respuesta) => {
        if (respuesta)
        {
            console.log(respuesta)
        }
    } )