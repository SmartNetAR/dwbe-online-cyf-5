const express = require('express')
const { sequelize } = require('./src/connection/sequelize')
const { User, Task } = require('./src/models/User')
const { getTareas } = require('./src/repositories/tareasRepository')

const app = express()
const port = 3000

app.use(express.json())

    ; (async () => {
        await sequelize.sync()
    })()

app.get('/users/', async (req, res) => {

    const userResult = await User.findAll({
        include: [
            {
                model: Task,
                as: "tasks"
            }
        ]
    })

    res.json(userResult)
})

app.get('/users/:id', async (req, res) => {

    const userResult = await User.findByPk(req.params.id, {
        include: [
            {
                model: Task,
                as: "tasks"
            }
        ]
    })

    res.json(userResult)
})

app.get('/tareas/', async (req, res) => {
    const { terminadas } = req.query

    const tareas = await getTareas(terminadas)

    res.json({ tareas })
})

app.post('/tareas/', async (req, res) => {
    const { titulo, descripcion } = req.body

    const tarea = await sequelize.query(`INSERT INTO tareas (titulo, descripcion) VALUES ("${titulo}", "${descripcion}")`,
        { type: sequelize.QueryTypes.INSERT })

    res.json({ tarea })
})

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})