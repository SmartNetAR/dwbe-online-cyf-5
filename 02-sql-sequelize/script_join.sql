SELECT eventos.nombre AS evento, inscriptos.nombre, inscriptos.email, inscriptos.telefono, inscriptos.empresa 
FROM eventos
LEFT JOIN inscriptos ON eventos.id = inscriptos.evento_id 
ORDER BY inscriptos.nombre ASC
LIMIT 1
OFFSET 2;