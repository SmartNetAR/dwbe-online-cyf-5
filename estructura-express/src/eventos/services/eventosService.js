
exports.getAllEvents = async (repository) => {
    const events = await repository.getAll();

    // const invitados = await eventosRepository.getAllInvited();

    // events.invitados = invitados;

    return events;
}


exports.getEvent = async (repository, {id}) => {
    const event = await repository.getById(id);

    return event;
}


exports.saveEvent = async (repository, event) => {
    const eventExists = await repository.getByName(event.nombre);

    if (eventExists) {
        throw new Error('Ya existe un evento con ese nombre');
    }

    const newEvent = await repository.save(event);

    return newEvent;
}