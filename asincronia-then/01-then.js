const axios = require('axios').default;

const usuario = "ogranada"
axios.get("https://api.github.com/users/"+usuario)  // usamos fetch en el browser como ejemplo
  // .then((respuestaUsuario)=>{                    // con axios no hace falta usar then para obtener la respuesta en json
  //   return respuestaUsuario.json()
  // })
  .then((githubUser)=>{
    axios.get(githubUser.data.followers_url)
      // .then((respuestaFollowers)=>{
      //   return respuestaFollowers.json()
      // })
      .then((followers) =>{
        console.log("Los followers son:", followers.data)
    })
  })
  .catch((error)=>{
    console.log(error)
  })