const eventosService = require('../services/eventosService')

exports.getAll = (repository) => async (req, res) => {
    const eventos = await eventosService.getAllEvents(repository)

    res.status(200).json(eventos)
}

exports.getById = (repository) => async (req, res) => {
    const id = req.params.id;
    
    if (isNaN(id)) {
        res.status(400).json({
            msg: "El id debe ser numerico"
        })
    }

    const evento = await eventosService.getEvent(repository, {id})

    if (!evento) {
        res.status(404).json({
            msg: "El evento no existe"
        })
    }

    res.status(200).json(evento)
}

exports.save = (repository) => async (req, res) => {
    const {nombre, fecha, hora} = req.body;
    
    if (!nombre || !fecha || !hora) {
        res.status(400).json({
            msg: "Todos los campos son obligatorios"
        })
    }

    const newNewEvent = {
        nombre,
        fecha,
        hora
    }

    const evento = await eventosService.saveEvent(repository, newNewEvent)

    res.status(201).json(evento)
}
