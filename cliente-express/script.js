
let $app = document.getElementById("app")

fetch( "http://localhost:3000/autos" )
    .then( (response) => response.json()  )
    .then( data => {
        const listaAutos = data.map( ( auto ) => `<li>Marca: ${auto.nombre} / Modelo ${auto.modelo}</li>` ).join('')
        $app.innerHTML = listaAutos
    } )