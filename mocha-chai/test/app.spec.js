
const chai = require('chai');
const expect = chai.expect;
const request = require('supertest');
const app = require('../src/app');

describe('#app.js', function(){
    describe('Home "/"', function() {
        it('Debería devolver el status code "200"', function(done) {

            request(app)
                .get('/')
                .end( function(err, res) {
                    expect(res.statusCode).to.equal(200);
                    done();
                })

        })
    })

    describe('Login "/login"', function() {
        it('Debería devolver el status code "401" cuando no le estamos enviado credenciales', function(done) {

            request(app)
                .post('/login')
                .end(function(err, res){
                    expect(res.statusCode).to.equal(401);
                    done();
                })
        })

        it('Debería devolver el status code "401" cuando las credenciales sean inválidas', function(done) {

            const payload = {
                email: "test",
                password: "secret"
            }

            request(app)
                .post('/login')
                .send(payload)
                .end(function(err, res){
                    expect(res.statusCode).to.equal(401);
                    done();
                })
        })

        it('Debería devolver el status code "201" y un token cuando las credenciales sean válidas', function(done) {

            const payload = {
                email: "leo@email.com",
                password: "1234"
            }

            request(app)
                .post('/login')
                .send(payload)
                .end(function(err, res){
                    const token = res.body.data.token;

                    expect(res.statusCode).to.equal(201);
                    expect(token).to.be.a('string');
                    expect(token).to.have.lengthOf.above(0);
                    done();
                })
        })
    })
})