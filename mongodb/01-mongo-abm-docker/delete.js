const mongoose = require('./connection')

const Pelicula = mongoose.model('peliculas', {
    titulo: String,
    director: String,
    genero: String,
    lanzamiento: Date
})

Pelicula.deleteOne({_id: '6137e44e26743eeaf7bf1376'})
    .then( (error, resp) => {
        if (error)
        {
            console.log(error)
        }
        console.log(resp)
    } )