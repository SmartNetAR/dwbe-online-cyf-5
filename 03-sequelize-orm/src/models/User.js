const { Model, DataTypes } = require("sequelize");
const { sequelize } = require("../connection/sequelize")


class User extends Model { }

User.init({
    nombre: DataTypes.STRING,
    apellido: DataTypes.STRING
}, { sequelize, modelName: "users" });


class Task extends Model { }

Task.init({
    titulo: DataTypes.STRING,
    descripcion: DataTypes.STRING,
    terminada: DataTypes.BOOLEAN
}, {sequelize, modelName: "tasks"});

User.hasMany( Task, { as: "tasks"});


// (async () => {
//     await sequelize.sync();

//     /*  
//     const user = await User.create({
//         nombre: "Juan",
//         apellido: "Gonzalez"
//     });

//     const task = await Task.create({
//         titulo: "Tarea 2",
//         descripcion: "descripción de la segunda tarea",
//         terminada: false,
//         userId: user.id
//     });
//     */


//     const userResult = await User.findOne({
//         where: {
//             nombre: "Juan"
//         },
//         include: [
//             {
//                 model: Task,
//                 as: "tasks"
//             }
//         ]
//     })


//     console.log(userResult.toJSON());

// })()

module.exports = {
    User,
    Task
}