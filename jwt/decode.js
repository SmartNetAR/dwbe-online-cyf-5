
const jwt = require("jsonwebtoken");

const token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.\
eyJpZCI6MTIzLCJuYW1lIjoianVhbiBtYXJ0aW5leiIsImlhdCI6MTYzMzQ3Nzk5N30.\
HYCKK7MeRbHC1Q0b2XO-jZwtv3P8VbT94InogU4at8ca"

try {
    
    const decoded = jwt.verify(token, 'otra_cosa');

    console.log("id de usuario:",decoded.id)

} catch (error) {
    
    console.log("NO ESTÁS AUTORIZADO")
}